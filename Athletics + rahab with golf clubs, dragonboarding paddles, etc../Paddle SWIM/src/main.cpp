#include <Adafruit_NeoPixel.h>

#define LED_PIN D7
#define NUM_LEDS 144

//111 111 11 
//r    g   b



Adafruit_NeoPixel strip(NUM_LEDS, LED_PIN, NEO_GRB + NEO_KHZ800);


#include <esp_now.h>
#include <WiFi.h>


// REPLACE WITH THE MAC Address of your receiver
//  48:31:b7:3e:8d:44 this is the miniESP32 C3

// uint8_t broadcastAddress[] = {0x48,0x31,0xb7,0x3e,0x8d,0x44};// 0x30,0xc6,0xf7,0x20,0x23,0x84};
// uint8_t broadcastAddress[] = {0x30,0xc6,0xf7,0x20,0x23,0x84};
//34:85:18:ac:08:28 is for the transmitter ESP

// Define variables to store incoming readings
byte ledState[72];

//Structure example to send data
//Must match the receiver structure
typedef struct struct_message {
    byte incomingLED[72];
} struct_message;


// Create a struct_message to hold incoming sensor readings
struct_message incomingLEDMessage;

esp_now_peer_info_t peerInfo;


// Callback when data is received
void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len) {
  memcpy(&incomingLEDMessage, incomingData, sizeof(incomingLEDMessage));
  // Serial.print("Bytes received: ");
  // Serial.println(len);
  memcpy(ledState, incomingLEDMessage.incomingLED, sizeof(incomingLEDMessage.incomingLED));
  // Serial.print("This just in (HEX values)... ");
  // for (byte n = 0; n < NUM_LEDS; n++) {
  //     Serial.print(ledState[n], BIN);
  //     Serial.print(' ');
  // }
  // Serial.println();
  // Serial.println(incomingLED, BIN);
}
 

void displayLED(){ //take care of the wrapping thing
// for(int i = 0; i < 144; i++){
//   strip.setPixelColor(i, 255, 255, 255);
// }
  for(int i = 0; i < 72; i++) {
    int r = ((uint) ((ledState[i] >> 5) & 0b111) / 7.0) * 255;
    int g = ((uint) ((ledState[i] >> 2) & 0b111) / 7.0) * 255;
    int b = ((uint) (ledState[i] & 0b11) / 3.0) * 255;
    // Serial.print(r);  Serial.print('\t');   Serial.print(g);   Serial.print('\t'); Serial.println(b);
    strip.setPixelColor(i, r, g, b);
    strip.setPixelColor(71 + (72 - i), r, g, b);
    // strip.setPixelColor(i, 255, 255, 255);
  }
  strip.show();
}


void setup() {
  // put your setup code here, to run once:
  strip.begin();           
  strip.show();            
  strip.setBrightness(50);
  Serial.begin(115200);
   // Initialize Serial Monitor
  Serial.begin(115200);
  
  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);

  // Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }
  
  // Once ESPNow is successfully Init, we will register for recv CB to
  // get recv packer info
  esp_now_register_recv_cb(OnDataRecv);
}

void loop() {
  displayLED();
  // strip.show();
  // put your main code here, to run repeatedly:
  
}


// /*
//   Rui Santos
//   Complete project details at https://RandomNerdTutorials.com/esp-now-esp32-arduino-ide/
  
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files.
  
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
// */

// #include <esp_now.h>
// #include <WiFi.h>

// // Structure example to receive data
// // Must match the sender structure
// typedef struct struct_message {
//     char a[32];
//     int b;
//     float c;
//     bool d;
// } struct_message;

// // Create a struct_message called myData
// struct_message myData;

// // callback function that will be executed when data is received
// void OnDataRecv(const uint8_t * mac, const uint8_t *incomingData, int len) {
//   memcpy(&myData, incomingData, sizeof(myData));
//   Serial.print("Bytes received: ");
//   Serial.println(len);
//   Serial.print("Char: ");
//   Serial.println(myData.a);
//   Serial.print("Int: ");
//   Serial.println(myData.b);
//   Serial.print("Float: ");
//   Serial.println(myData.c);
//   Serial.print("Bool: ");
//   Serial.println(myData.d);
//   Serial.println();
// }
 
// void setup() {
//   // Initialize Serial Monitor
//   Serial.begin(115200);
  
//   // Set device as a Wi-Fi Station
//   WiFi.mode(WIFI_STA);

//   // Init ESP-NOW
//   if (esp_now_init() != ESP_OK) {
//     Serial.println("Error initializing ESP-NOW");
//     return;
//   }
  
//   // Once ESPNow is successfully Init, we will register for recv CB to
//   // get recv packer info
//   esp_now_register_recv_cb(OnDataRecv);
// }
 
// void loop() {

// }